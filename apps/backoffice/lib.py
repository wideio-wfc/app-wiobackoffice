# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################


import settings
import logging

"""
This should not be used for webserver internal logging.
"""


def log_init():
    try:
        handler = logging.handlers.SysLogHandler(address=getattr(settings, "SYS_LOG", "/dev/log"))
        logging._addHandlerRef(handler)
    except:
        pass

def _log_fatal(txt, user=None):
    from backoffice.models import Log
    o = Log()
    o.message = txt[:Log.MAX_LENGTH]
    o.level = 0
    if (user is not None) and (not user.is_anonymous()):
        o.user = user
    if settings.ALLOW_LOG_FAILURE:
        try:
            o.save()
        except:
            print "Failed to write log:"
            print txt
    else:
        o.save()


def _log_error(txt, user=None):
    from backoffice.models import Log
    o = Log()
    o.message = txt[:Log.MAX_LENGTH]
    o.level = 1
    if (user is not None) and (not user.is_anonymous()):
        o.user = user
    if settings.ALLOW_LOG_FAILURE:
        try:
            o.save()
        except:
            print "Failed to write log:"
            print txt
    else:
        o.save()


def _log_warning(txt, user=None):
    from backoffice.models import Log
    o = Log()
    o.message = txt[:Log.MAX_LENGTH]
    o.level = 2
    if (user is not None) and (not user.is_anonymous()):
        o.user = user
    if settings.ALLOW_LOG_FAILURE:
        try:
            o.save()
        except:
            print "Failed to write log:"
            print txt
    else:
        o.save()


def log_info(txt, user=None):
    from backoffice.models import Log
    o = Log()
    o.message = txt[:Log.MAX_LENGTH]
    o.level = 3
    if (user is not None) and (not user.is_anonymous()):
        o.user = user
    if settings.ALLOW_LOG_FAILURE:
        try:
            o.save()
        except:
            print "Failed to write log:"
            print txt
    else:
        o.save()


def _log_debug(txt, user=None):
    from backoffice.models import Log
    o = Log()
    o.message = txt[:Log.MAX_LENGTH]
    o.level = 4
    if (user is not None) and (not user.is_anonymous()):
        o.user = user
    if settings.ALLOW_LOG_FAILURE:
        try:
            o.save()
        except:
            print "Failed to write log:"
            print txt
    else:
        o.save()


## simply use logging
import settings

log_init()


def log_debug(txt, user=None):
    logging.debug(txt)


def log_warning(txt, user=None):
    logging.warning(txt)


def log_info(txt, user=None):
    logging.info(txt)


def log_error(txt, user=None):
    logging.error(txt)


def log_fatal(txt, user=None):
    logging.error(txt)
