# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
"""
Backoffice cron jobs

Note: cron.py file in app folder is exectued by django_cron app, for futher information refer to https://code.google.com/p/django-cron/

***********
WARNING : Experimental
***********

"""

import datetime
from wioframework.django_cron.base import cronScheduler, CronJob
import backoffice.lib as debug
import django.db


# class TestJob(Job):
#         """ Test job for cron to check installation """
#         run_every = 30
#         def job(self):
#             # This will be executed every 5 minutes
#             debug.log_info("Got executed")
#
# cronScheduler.register(TestJob)


class DeleteExpiredJob(CronJob):
    """ Job to clean database from wideio_timestamped with expired field """
    run_every = 20  # s TODO: find best time period

    def job(self):
        # Executed every run_every seconds #
        current_datetime = datetime.datetime.now()
        mydebug = "Deleting expiring objects\n"
        # print mydebug
        # Iterate through every model #o
        # django.db.models.get_models() doesnt work, even with
        # include_auto_created=True
        model_list = sum(
            [
                django.db.models.get_models(
                    a,
                    include_auto_created=True) for a in django.db.models.get_apps()],
            [])  # FIXME: models.get_models() doesn't return all the models, why?
        # not included in INSTALLED_APPS FIXME still should be working
        model_list += django.db.models.get_models(
            django.db.models.get_app("references"))

        for model in model_list:
            if hasattr(model, "expires_at"):
                mydebug += "Check {0}".format(model.__name__)
                expiring_objects = model.objects.filter(
                    expires_at__lte=current_datetime,
                    wiostate__startswith="U")  # filter expiring objects
                mydebug += str(expiring_objects)
                for expiring_object in expiring_objects:
                    if hasattr(expiring_object, "on_expire"):
                        expiring_object.on_expire()
                    if hasattr(expiring_object.WIDEIO_Meta,
                               "delete_database_entry_on_expire"):
                        if expiring_object.WIDEIO_Meta.delete_database_entry_on_expire:
                            # delete expiring objects only if
                            # WIDEIO_Meta.deleteon_expire = true
                            expiring_object.delete()


cronScheduler.register(DeleteExpiredJob)
