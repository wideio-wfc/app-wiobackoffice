# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# import datetime
from django.db import models
from django.core.urlresolvers import reverse

from django.contrib.auth.models import AbstractUser  # , User
from wioframework.amodels import wideiomodel, wideio_timestamped, get_dependent_models, wideio_timestamped_autoexpiring, wideio_publishable, wideio_action, wideio_owned, wideio_commentable, wideio_setnames
from wioframework import decorators as dec

from wioframework.fields import JSONField
import settings
import datetime

@wideio_timestamped
@wideiomodel
@wideio_setnames("motto of the day", "mottos of the day")
class MotOfTheDay(models.Model):
    """
    The mots of the day displayed on the homepage.
    """
    name = models.CharField(
        blank=True,
        max_length=128,
        db_index=True,
        unique=True)
    content = models.TextField(blank=True)
    published = models.DateField(blank=True, null=True)

    def __unicode__(self):
        return self.name

    def send(self, request):
        from accounts.models import UserAccount
        self.send_to(request, UserAccount.objects.filter(is_active=True))

    def send_to(self, request, to):
        from network.models import Announcement
        Announcement.create_internal(request.user, "Message from the WIDE IO Team", content=self, comments_opened=False, to=to)
        if not self.published:
            self.published = datetime.datetime.now()

    class WIDEIO_Meta:
        NO_DRAFT = True
        permissions = dec.perm_write_for_staff_only
        DISABLE_VIEW_ACCOUNTING = True
        form_exclude = ["published"]
        class Actions:
            @wideio_action(possible=lambda s,r:r.user.is_staff)
            def send_to_all_active_users(self,request):
                self.send(request)
                return "alert('done');"

        MOCKS = {
            'default': [
                {
                    'name': 'Today\'s quote',
                    'content': 'Murphy was an optimist',
                    'published': datetime.datetime(2016,1,1)
                }
            ]
        }