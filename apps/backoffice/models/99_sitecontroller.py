# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# import datetime
from django.db import models
from django.core.urlresolvers import reverse

from django.contrib.auth.models import AbstractUser  # , User
from wioframework.amodels import wideiomodel, wideio_timestamped, get_dependent_models, wideio_timestamped_autoexpiring, wideio_publishable, wideio_action, wideio_owned, wideio_commentable, wideio_setnames
from wioframework import decorators as dec

from wioframework.fields import JSONField
import settings
import os

# @wideiomodel
# @wideio_setnames("site controller")
# class SiteController(models.Model):
#     name = models.TextField(max_length=128)
#     is_active = models.BooleanField(default=True)
#     is_tenant = models.BooleanField(default=True)
#     data = JSONField()
#
#     def __unicode__(self):
#         return self.name
#
#     def get_flat_name(self):
#         return self.name.lower().replace(" ", "")
#
#     @staticmethod
#     def get_by_flat_name(name):
#         for sc in SiteController.objects.all():
#             if sc.get_flat_name() == name:
#                 return sc
#
#     @staticmethod
#     def can_list(request):
#         return request.user and request.user.is_staff
#
#     @staticmethod
#     def can_add(request):
#         return request.user.is_staff
#
#     @staticmethod
#     def get_current(request):
#         if request.tenant:
#             return SiteController.objects.get(name=request.tenant)
#         else:
#             return SiteController.objects.get(is_tenant=False)
#
#     def get_logo(self):
#         return self._metadata['logo_path'] if self._metadata else self._get_logo_path('wideio')
#
#     @staticmethod
#     def _get_logo_path(name):
#         logo_dir = os.path.join(settings.MEDIA_ROOT, 'images/logos')
#         return os.path.join(logo_dir, name+'.png')
#
#     class WIDEIO_Meta:
#         permissions = dec.perm_for_staff_only
#         NO_MOCKS=True
#         class Actions:
#             @wideio_action(possible=lambda x,r: x.is_active,icon="icon-cogs")
#             def recompile_media(self, request):
#                 import subprocess, os
#                 cmd = "./recompile_media.sh"
#                 if self.is_tenant:
#                     cmd += " --tenant="+self.get_flat_name()
#                 # process = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
#                 # (stdout, stderr) = process.communicate()
#                 return "prompt('Run command to recompile this media (automated function in development)', '" + cmd + "');"
#                 stdout = os.system(cmd)
#                 # for now! FIXME
#                 return "returned with exit code: "+stdout
#             @wideio_action(possible=lambda x,r:True, icon="icon-cogs")
#             def recompute_metadata(self, request):
#                 if not self._metadata:
#                     self._metadata = {}
#                 logo_path = self._get_logo_path(self.get_flat_name())
#                 if os.path.isfile(logo_path):
#                     self._metadata['logo_path'] = '/'+logo_path
#                 else:
#                     self._metadata['logo_path'] = '/'+self._get_logo_path("wideio")
#                 self.save()
#                 return "alert('Complete');"