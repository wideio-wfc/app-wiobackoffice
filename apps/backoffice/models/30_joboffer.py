# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# import datetime
from django.db import models
from django.core.urlresolvers import reverse

from django.contrib.auth.models import AbstractUser  # , User
from wioframework.amodels import wideiomodel, wideio_timestamped, get_dependent_models, wideio_timestamped_autoexpiring, wideio_publishable, wideio_action, wideio_owned, wideio_commentable, wideio_setnames
from wioframework import decorators as dec

from wioframework.fields import JSONField
import settings


@wideio_publishable()
@wideio_timestamped
@wideiomodel
class JobOffer(models.Model):

    """
    The mots of the day displayed on the homepage.
    """
    title = models.CharField(
                                blank=True,
                                max_length=128,
                                db_index=True,
                                unique=True)
    department          = models.CharField(max_length=64,blank=True,choices=map(lambda x:(x,x),['Management & Business Development','Web Platform Team','Cloud Platform Team','Data Science Team','Sales','Marketing & Design']))
    type_of_contract    = models.CharField(max_length=64,blank=True,choices=map(lambda x:(x,x),['Permanent position','Part-Time','Fix Term','Flexible hours','Internship','Apprenticeship']))
    mission             = models.TextField(help_text="Role definition is very important for new candidates to understand where they sit in the organisation.")
    responsibility      = models.TextField(blank=True,help_text="Help people to understand what their level of responsibility - people must be responsible at some point.")
    required_education  = models.TextField(blank=True,default="Revelent Master Degree - PhD preferred.")
    required_experience = models.TextField(blank=True,default="Revelent Master Degree - PhD preferred.")
    required_skills     = models.TextField(blank=True)
    good_to_have        = models.TextField(blank=True)
    extra_benefits      = models.TextField(blank=True)
    location            = models.TextField(blank=True,default="Penn Street Office")
    image               = models.ForeignKey('references.Image',null=True)

    def get_all_references(self,stack):
      return [self.image]

    def __unicode__(self):
        return self.title

    class WIDEIO_Meta:
        permissions = dec.perm_write_for_admin_only
        MOCKS={
            'default':[
                {
                'title': 'Dr',
                'department': 'Technology',
                'type_of_contract': 'Permanent',
                'mission': 'Help develop algorithms',
                'responsibility': 'algorithms',
                'required_education': 'PhD in CS',
                'required_experience': '2.y. in industry'
                }
            ]
        }