# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# Create your views here.
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from wioframework.decorators import login_required, user_passes_test
from django.template.loader import render_to_string
from django.conf import settings
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, RequestContext
from django.template import defaultfilters
from datetime import datetime, time, timedelta
from wioframework import utilviews as uv
from wioframework.pyplot_views import pyplot_view
from accounts.models import UserAccount
from backoffice import models
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
import os
import subprocess
import json
import settings
from functools import reduce


def add_tquery(now, t, field):
    if (t == "n"):
        return {}
    if (t.endswith('d')):
        return {field + "__gt": now - timedelta(int(t[:-1]))}
    if (t.endswith('h')):
        return {field + "__gt": now - timedelta(int(0, 3600 * t[:-1]))}


def gen_counts(
        now, queryset, when=['n', '1d', '7d', '31d'], field="created_at"):
    return (
        when,
        map(lambda t: queryset.filter(
            **(add_tquery(now, t, field))).count(), when)
    )


@user_passes_test(lambda u: u.is_superuser)
@pyplot_view()
def count_history(request, f, a):
    import numpy
    import json
    from matplotlib import pyplot
    import django.db.models.loading
    import accounts.models
    args = (
        request.REQUEST.get(
            'model',
            'accounts.UserAccount')).split(".")
    field = request.REQUEST.get('field', "date_joined")
    filter_constraint = json.loads(
        request.REQUEST.get(
            'filter_constraint',
            "{}"))
    model = django.db.models.loading.get_model(*args)
    if (model is None):
        model = getattr(
            __import__(
                args[0] +
                ".models",
                fromlist=[
                    args[0]]),
            args[1])
    # q1=accounts.models.UserAccount.objects.filter(is_scientist=True)
    if (len(filter_constraint.items())):
        q1 = model.objects.filter(**filter_constraint)
    else:
        q1 = model.objects.all()
    n = datetime.utcnow()
    x = numpy.arange(100)

    def get_c(t):
        kw = {field + "__lt": n - timedelta(int(t))}
        return q1.filter(**kw).count()
    r = map(get_c, x)
    a.plot(x, r[::-1])
    #f.title("Number of scientific users")
count_history.default_url = 'count_history'


@user_passes_test(lambda u: u.is_superuser)
def dashboard(request):
    """
    This view shows how well the business is going.
    It is designed for us to understand who are our users...
    Check that we grow as predicted
    """
    n = datetime.utcnow()
    import science.models
    import cloud.models
    import compute.models
    import payment.models
    import accounts.models
    import references.models

    analytics = [
        ('REGISTRATION', [
            ('Companies',
             gen_counts(n,
                        UserAccount.objects.filter(
                            is_customer=True),
                        field="date_joined"),
                'accounts.UserAccount',
             'date_joined'),
            ('Scientists',
                gen_counts(n,
                           UserAccount.objects.filter(
                               is_scientist=True),
                           field="date_joined"),
                'accounts.UserAccount',
                'date_joined'),
            #('lab_counts',gen_counts(n,science.models.Lab.objects),'accounts.Lab'),
        ]
        ),
        ('MATCHMAKING', [
            ('Questions',
             gen_counts(n,
                        science.models.Question.objects),
             'science.Question',
             'created_at'),
            ('Algorithms',
             gen_counts(n,
                        science.models.Algorithm.objects),
             'science.Question',
             'created_at'),
            #        ('Products',gen_counts(n,science.models.IndustrialProduct.objects),'science.IndustrialProduct','created_at'),
        ]
        ),
        ('COMPUTE', [
            ('ExecRequest',
             gen_counts(n,
                        compute.models.Job.objects),
             'compute.ExecRequest',
             'created_at'),
            ('Jobs',
             gen_counts(n,
                        compute.models.Job.objects),
             'compute.Job',
             'created_at'),
        ]
        ),
        ('PAYMENT', [
            ('Transactions',
             gen_counts(n,
                        payment.models.Transaction.objects),
             'payment.Transaction',
             'created_at'),
        ]
        ),
        ('REFERENCES', [
            #('Files',gen_counts(n,references.models.File.objects),'references.File','created_at'),
            ('Images',
             gen_counts(n,
                        references.models.Image.objects),
             'references.Image',
             'created_at'),
        ]
        ),
    ]

    context = {
        'request': request,
        'analytics': analytics
    }
    return render_to_response(
        "backoffice/statistics.html", context, RequestContext(request))
dashboard.default_url = 'dashboard'


@user_passes_test(lambda u: u.is_superuser)
def monitor(request):
    context = {'request': request}
    return render_to_response(
        "backoffice/monitor.html", context, RequestContext(request))
monitor.default_url = 'monitor'


@pyplot_view()
def simple_curve(request, f, a):
    import numpy
    from matplotlib import pyplot
    x = numpy.arange(100)
    a.plot(x, x**2)
simple_curve.default_url = 'simple_curve'


@user_passes_test(lambda u: u.is_superuser)
def manage_blurbtexts(request):
    if (not os.path.exists(settings.WIDEIO_BACKUP_PATH)):
        os.system("mkdir -p " + settings.WIDEIO_BACKUP_PATH)
    backups = []
    try:
        backups = os.walk(
            os.path.join(
                settings.WIDEIO_BACKUP_PATH,
                'blurbtext')).next()[1]
    except Exception as e:
        print e
    context = {'request': request, 'backups': backups}
    return render_to_response(
        "backoffice/manage_blurbtexts.html", context, RequestContext(request))
manage_blurbtexts.default_url = 'blurbtext/manage'


@user_passes_test(lambda u: u.is_superuser)
def dump_blurbtexts(request):
    import datetime
    django_cred = settings.MONGO_CONNECT
    django_user = django_cred["USER"]
    django_pw = django_cred["PASSWORD"]
    dbname = settings.DATABASENAME
    savedate = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M")
    subprocess.call(["mongodump",
                     "--username",
                     django_user,
                     "--password",
                     django_pw,
                     "--db",
                     dbname,
                     "--collection",
                     "backoffice_blurbtext",
                     "--out",
                     os.path.join(settings.WIDEIO_BACKUP_PATH,
                                  'blurbtext',
                                  savedate + ".bson")])

    from django.contrib import messages
    messages.add_message(
        request,
        messages.INFO,
        "Text of the blurbtexts saved.")
    return HttpResponseRedirect("../manage")
dump_blurbtexts.default_url = 'blurbtext/dump'


@user_passes_test(lambda u: u.is_superuser)
def load_blurbtexts(request, filename):
    django_cred = settings.MONGO_CONNECT
    django_user = django_cred["USER"]
    django_pw = django_cred["PASSWORD"]
    dbname = settings.DATABASENAME
    subprocess.call(["mongorestore",
                     "--username",
                     django_user,
                     "--password",
                     django_pw,
                     "--db",
                     dbname,
                     "--drop",
                     "--collection",
                     filename])
    from django.contrib import messages
    messages.add_message(
        request,
        messages.INFO,
        "Text of the blurbtexts restored to the " +
        filename +
        "version.")
    return HttpResponseRedirect("../../manage")
load_blurbtexts.default_url = 'blurbtext/load/([^/]+)'

# def users_list(request):
#	all_users_list =
#	return	HttpResponse(all_users_list)


def gim(request, results=4):
    import json
    import urllib
    import urllib2
    urls = []
    reqresults = []
    query = request.REQUEST["q"]
    for start in range(0, results, 4):
        r = json.loads(
            urllib2.urlopen(
                'http://ajax.googleapis.com/ajax/services/search/images?v=1.0&%s' %
                (urllib.urlencode(
                    {
                        'q': query,
                        'start': start}),
                 )).read())
        reqresults.append(r["responseData"]["results"])
        urls = urls + map(lambda r: r["url"], reqresults[-1])
    return HttpResponse(json.dumps(urls))
gim.default_url = "gim"

VIEWS = reduce(lambda x, y: x + uv.AUDLV(y), models.MODELS, [])

# VIEWS=(
# uv.AUDLV(models.BlurbText, all_decorators=[user_passes_test(lambda u: u.is_superuser),csrf_exempt])
# + uv.AUDLV(models.Log, list_add_enabled=False ,all_decorators=[user_passes_test(lambda u: u.is_superuser),csrf_exempt])
# )
VIEWS += [dashboard,
          simple_curve,
          count_history,
          manage_blurbtexts,
          dump_blurbtexts,
          load_blurbtexts,
          monitor]


def eactive(e):
    return (("stop" not in e) or (e["stop"] == None) or (
        e["stop"] > datetime.now()))


def startstopup(e):
    es = e["start"]
    e["start"] = (datetime(es[2], es[1], es[0]) if es else es)
    es = e["stop"]
    e["stop"] = (datetime(es[2], es[1], es[0]) if es else es)
    return e


def TEAM(request, modelname):
    try:
        from backoffice.finance import update_model_assumptions_according_to_cookies
        model = __import__(
            'aiosciweb1.backoffice.finance.' +
            modelname,
            fromlist=[
                'aiosciweb1',
                'backoffice',
                'finance'])
        update_model_assumptions_according_to_cookies(model, request)
        EMPLOYEES = model.staff.EMPLOYEES
    except:
        import json
        EMPLOYEES = json.loads(
            file(
                os.path.join(
                    settings.CWD,
                    "data",
                    modelname +
                    "-staff.json"),
                "r").read())
        EMPLOYEES = map(startstopup, EMPLOYEES)

    context = {'directors': filter(lambda e: e['contract']in['director', 'executive'], EMPLOYEES),
               'advisers': filter(lambda e: e['contract'] == 'adviser', EMPLOYEES),
               'consultants': filter(lambda e: e['contract'] == 'consultant', EMPLOYEES),
               'employees': filter(lambda e: e['contract'] not in ['director', 'adviser', 'consultant', 'outsourced', 'volunteer', 'shareholder', 'investor', 'executive'], EMPLOYEES),
               'friends': filter(lambda e: e['contract'] in ['volunteer', 'outsourced'], EMPLOYEES),
               }
    #context=dict(map(lambda i:(i[0],map(lambda z:("staff-active",z),filter(eactive,i[1]))+map(lambda z:("staff-inactive",z),filter(lambda x:(not eactive(x)),i[1])) ),context.items()))
    ocontext = context
    context = dict(map(lambda i: ("old_" + i[0],
                                  map(lambda z: ("staff-inactive",
                                                 z),
                                      filter(lambda x: (not eactive(x)),
                                             i[1]))),
                       ocontext.items()))
    context.update(dict(map(lambda i: (i[0], map(
        lambda z: ("staff-active", z), filter(eactive, i[1]))), ocontext.items())))
    context = dict(map(lambda i: (i[0], map(lambda e: (e, lambda: render_to_string(
        "finance/finance_team_staff.html", {'e': e, 'request': request})), i[1])), context.items()))
    return render_to_response(
        "finance/finance_team.html", context, RequestContext(request))
TEAM.default_url = 'finance/([^/]+)/team'
VIEWS.append(TEAM)


try:
    import backoffice.finance.views
    VIEWS = VIEWS + backoffice.finance.views.VIEWS
except Exception as e:
    print e
