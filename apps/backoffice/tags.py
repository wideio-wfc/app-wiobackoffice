# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
#
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
#
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- coding: utf-8 -*-
import re
from django.template import Library, Node, TemplateSyntaxError
from django.template import Variable
from django.template.loader import render_to_string
from django.shortcuts import RequestContext

import settings

ipsum_lorem = """Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
est laborum."""

from wioframework.tagslib import register_tag

@register_tag()
def blurbtext_content(parser, token):
    bits = token.split_contents()

    class TNode(Node):
        def render(self, context):
            prefix = ""
            user = (Variable(bits[1])).resolve(context)
            blurbid = bits[2]
            from backoffice.models import BlurbText
            qs = BlurbText.objects.filter(name=blurbid)
            if qs.count() == 0:
                return ipsum_lorem
            return qs[0].content
    return TNode()



@register_tag()
def blurbtext_id(parser, token):
    bits = token.split_contents()

    class TNode(Node):

        def render(self, context):
            blurbid = bits[2]
            from backoffice.models import BlurbText
            qs = BlurbText.objects.filter(name=blurbid)
            return qs[0].id
    return TNode()

@register_tag()
def blurbtext(parser, token):
    bits = token.split_contents()
    class TNode(Node):

        def render(self, context):
            blurbid = bits[1]
            blurbid = re.sub(
                r"\{\{([^}]+)\}\}",
                lambda m: str(
                    Variable(
                        m.group(1)).resolve(context)),
                blurbid)
            request = (Variable("request")).resolve(context)
            default_txt = "<!-- No blurb text -->"
            if len(bits) > 2:
                default_txt = (Variable(bits[2])).resolve(context)

            user = request.user
            from backoffice.models import BlurbText
            qs = BlurbText.objects.filter(name=blurbid)
            if qs.count() == 0:
                b = BlurbText()
                b.name = blurbid
                b.save()
                qs = BlurbText.objects.filter(name=blurbid)

            bt = qs[0]
            content = qs[0].content
            if not content:
                content = default_txt
            if (bt.can_update(request)):
                if not content:
                    content = "Edit me"
                return render_to_string("widgets/blurb_text_edit.html",
                                        {
                                        'blurbtext': content,
                                        'blurbid': blurbid,
                                        "x": qs[0],
                                        'added_model': 'blurbtext'
                                        }, RequestContext(request))
            return content
    return TNode()
